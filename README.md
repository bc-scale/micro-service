# Cloud project

This project aim to deploy frontend and backend as micro-services with kubernetes and terraform.

>**Kubernetes** uses docker images from Docker Hub.
#### Backend

This backend is an api which retrieves [Product Hunt](https://www.producthunt.com/) products from a date.

REPO : https://github.com/Li-Steven-pro/ProductHuntBackend
Docker hub : https://hub.docker.com/repository/docker/stevenli3/producthunt-api

#### Frontend

The frontend application is used with the backend to show and request products.

REPO : https://github.com/Li-Steven-pro/ProductHuntFrontend
Docker hub : https://hub.docker.com/repository/docker/stevenli3/producthunt-front

### Pre-requisites

- [Kubernetes](https://kubernetes.io/releases/download/) 
- [Docker](https://docs.docker.com/get-docker/)
- [Terraform](https://www.terraform.io/downloads) 
### Deploy project through Terraform

##### Install provider

```
terraform init
```

##### Execute Terraform plan
```
terraform apply
```

You can check the application on web browser using this url: localhost

##### Remove resources
```
terraform destroy
```

### Deploy project through only Kubernetes

##### Create backend deployment

```
kubectl apply -f backend-deploy.yaml
```
##### Create backend service
```
kubectl apply -f backend-service.yaml
```

##### Create frontend deployment
```
kubectl apply -f nginx-deploy.yaml
```

##### Create frontend service
```
kubectl apply -f nginx-service.yaml
```

#### Delete resources

```
kubectl delete services producthunt-api
kubectl delete services frontend-service
kubectl delete deployments backend
kubectl delete deployments frontend-deployment
```